<?php
/**
 * @file
 * Template file for webcam-item
 */
?>
<p class="webcam_field_widget_status"></p>
<div class="webcam_field_widget_webcam">

  <div id="<?php print $flash_id; ?>" class="media-webcam-flash">
    <?php print t("The Flash file can not load. You will not be able to capture an image before you have installed and enabled Flashplayer."); ?>
  </div>

<canvas id="<?php print ($canvas_id); ?>" class="webcam_field_widget_canvas" width="320" height="240" ></canvas>
</div>
<p>
<button type="button" class="media-webcam-button media-webcam-snap"><?php print t("Capture"); ?></button>
<button type="button" class="media-webcam-button media-webcam-delete"><?php print t("Delete"); ?></button>
<button type="button" class="media-webcam-button media-webcam-reset"><?php print t("Reset"); ?></button>
</p>
<h3><?php print t("Available Cameras"); ?></h3>

<ul class="webcam_field_widget_cams"></ul>

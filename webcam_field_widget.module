<?php

/**
 * @file
 * Webcam Image field widget
 */


/**
 * Implements hook_field_widget_info().
 */
function webcam_field_widget_field_widget_info() {
  return array(
    'webcam_field_widget' => array(
      'label'       => t('Webcam'),
      'field types' => array('image'),
      'behaviors'   => array(
        'multiple values' => FIELD_BEHAVIOR_DEFAULT,
        'default value'   => FIELD_BEHAVIOR_NONE,
      ),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function webcam_field_widget_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {

  $element['#element_validate']       = array('webcam_field_widget_validate');
  $element['#upload_location']        = file_field_widget_uri($field, $instance);
  $element['#upload_validators']      = webcam_field_widget_upload_validators($field, $instance);

  $field_id = $element['#field_name'] . '_' . $langcode . '_' . $delta;
  $fieldset_id = $element['#field_name'] . '_' . $langcode . '_' . $delta . '_fieldset';
  $image_field_id   = $element['#field_name'] . '_' . $langcode . '_' . $delta . '_image_field';
  $delete_id   = $element['#field_name'] . '_' . $langcode . '_' . $delta . '_delete';
  $canvas_id   = $element['#field_name'] . '_' . $langcode . '_' . $delta . '_canvas';
  $flash_id = $field_id . 'webcamObject';

  $element['image_fieldset'] = array(
    '#type'        => 'fieldset',
    '#title'       => check_plain($element['#title']),
    '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
    '#tree'        => TRUE,
    '#attributes'  => array(
      'id' => array($fieldset_id),
    ),
  );

  if (!empty($items[$delta])) {
    $image = $items[$delta];
    // When the form is generated in ajax, images are not loaded,
    // so the 'fid' can be sent without the uri being loaded.
    if (!empty($image['uri'])) {
      $image_uri = file_create_url($image['uri']);
    }
    if (!empty($image['fid'])) {
      $element['image_fieldset']['old_image_fid'] = array(
        '#type'  => 'value',
        '#value' => $image['fid'],
      );
    }
  }

  if (!empty($field['settings']['default_image'])) {
    $file_object = file_load($field['settings']['default_image']);
    if ($file_object) {
      $default_image_uri = file_create_url($file_object->uri);
    }
  }
  $url_flash_file = url(drupal_get_path('module', 'webcam_field_widget') . '/jscam_canvas_only.swf');
  $theme_variables = array(
    'flash_id'  => $flash_id,
    'canvas_id' => $canvas_id,
  );

  $js_libraries = array(
    drupal_get_path('module', 'webcam_field_widget') . '/webcam_field_widget.js' => array('type' => 'file'),
    'http://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js' => array('type' => 'external'),
  );

  $js_settings = array(
    'webcamFieldWidget' => array(
      'flash' => array(
        "$flash_id" => array(
          'id'           => ($flash_id),
          'fieldsetId'   => $fieldset_id,
          'webcamPath'   => $url_flash_file,
          'canvasId'     => $canvas_id,
          'deleteId'     => $delete_id,
          'imageFieldId' => $image_field_id,
          'fieldsetId'   => $fieldset_id,
        ),
      ),
    ),
  );
  if (!empty($image_uri)) {
    $js_settings['webcamFieldWidget']['flash']["$flash_id"]['imageUri'] = $image_uri;
  }
  if (!empty($default_image_uri)) {
    $js_settings['webcamFieldWidget']['flash']["$flash_id"]['defaultImageUri'] = $default_image_uri;
  }
  $js_lib_var = $js_libraries;
  $js_lib_var[] = array(
    'data' => $js_settings,
    'type' => 'setting',
  );

  $element['image_fieldset']['image'] = array(
    '#type'     => 'hidden',
  // '#required' => $element['#required'],
    '#attached' => array(
      'js' => $js_lib_var,
      'css' => array(
        drupal_get_path('module', 'webcam_field_widget') . '/webcam_field_widget.css',
      ),
    ),
    '#suffix'     => theme('webcam-item', $theme_variables),
    '#attributes' => array('id' => array($image_field_id)),
  );

  $element['image_fieldset']['delete'] = array(
    '#type'          => 'hidden',
    '#default_value' => 'FALSE',
    '#attributes'    => array('id' => array($delete_id)),
  );

  return $element;
}

/**
 * Retrieves the upload validators for a webcam_field_widget.
 *
 * @param array $field
 *          a field array
 *
 * @param array $instance
 *          the field instance
 *
 * @return array
 *   An array suitable for passing to _webcam_field_widget_file_validator() or
 *   file_save_upload() or the file field
 *   element's '#upload_validators' property.
 */
function webcam_field_widget_upload_validators($field, $instance) {
  $validators = file_field_widget_upload_validators($field, $instance);
  $settings = $instance['settings'];
  // Add upload resolution validation.
  if ($settings['max_resolution'] || $settings['min_resolution']) {
    $validators['file_validate_image_resolution'] = array($settings['max_resolution'], $settings['min_resolution']);
  }

  // If not using custom extension validation, ensure this is an image.
  $supported_extensions = array('png', 'gif', 'jpg', 'jpeg');
  $extensions = isset($validators['file_validate_extensions'][0]) ? $validators['file_validate_extensions'][0] : implode(' ', $supported_extensions);
  $extensions = array_intersect(explode(' ', $extensions), $supported_extensions);
  $validators['file_validate_extensions'][0] = implode(' ', $extensions);

  return $validators;
}

/**
 * Implements hook_theme().
 */
function webcam_field_widget_theme() {
  return array(
    'webcam-item' => array(
      'template' => 'webcam-item',
    ),
  );
}

/**
 * Validate and save webcam image.
 *
 * Saving the image in the validate function is some what hacky,
 * but the best way I've found so far to get the webcam widget
 * to work together with the image field.
 *
 * @param array $element
 *          the Form API element being validated
 *
 * @param array $form_state
 *          form state. Contains the values input in the form by the user.
 */
function webcam_field_widget_validate($element, &$form_state) {
  $values     = $form_state['values'];
  $language   = $element['#language'];
  $field_name = $element['#field_name'];
  $delta      = $element['#delta'];

  if (!empty($values[$field_name][$language][$delta]['image_fieldset']['delete'])) {
    $delete = $values[$field_name][$language][$delta]['image_fieldset']['delete'];
    if ($delete === 'TRUE') {
      return;
    }
  }

  // When no new image is captured.
  if (empty($values[$field_name][$language][$delta]['image_fieldset']['image'])) {

    if (!empty($values[$field_name][$language][$delta]['image_fieldset']['old_image_fid'])) {
      // Continue using the old image.
      $old_values['fid'] = $values[$field_name][$language][$delta]['image_fieldset']['old_image_fid'];
      form_set_value($element, $old_values, $form_state);
    }
    return;
  }

  $item = $values[$field_name][$language][$delta]['image_fieldset']['image'];

  // Convert the base64 encoded string to an image file.
  $file_data   = fopen($item, 'r');

  // Most of the file saving code is duplicate of function
  // "file_managed_file_save_upload($element)" from
  // core in file modules/file/file.module
  $destination = isset($element['#upload_location']) ? $element['#upload_location'] : NULL;
  if (isset($destination) && !file_prepare_directory($destination, FILE_CREATE_DIRECTORY)) {
    watchdog('file', 'The upload directory %directory for the image field !name could not be created or is not accessible. A newly captured image could not be saved in this directory as a consequence, and the upload was canceled.', array('%directory' => $destination, '!name' => $element['#field_name']));

    form_set_error(implode('_', $element['#parents']), t('The image could not be uploaded.'));
    return;
  }

  $number = variable_get('webcam_field_widget_' . $field_name, $default = 0);
  variable_set('webcam_field_widget_' . $field_name, $number + 1);
  $filename = 'webcam_field_widget_' . $field_name . '_' . $number . '.jpeg';
  $destination = file_create_filename($filename, $destination);

  if (!$file_object = file_save_data($file_data, $destination, FILE_EXISTS_RENAME)) {
    watchdog('file', 'The image upload failed. %upload', array('%upload' => implode('_', $element['#parents'])));
    form_set_error(implode('_', $element['#parents']), t('The image in the @name field was unable to be uploaded.', array('@name' => $element['#title'])));
    return;
  }

  if (!_webcam_field_widget_file_validator($file_object, $element['#upload_validators'])) {
    return;
  }

  $new_values = get_object_vars($file_object);
  form_set_value($element, $new_values, $form_state);
}

/**
 * Validate the saved webcam image.
 *
 * Code is duplicate of validation code from the
 * function file_save_upload in file.inc in core.
 * In the original image field widget the validation is handled by the
 * validations functions in the Form API's managed_file element.
 * Since this widget is build up of hidden form elements we can't hand
 * the validation of to the Form API.
 *
 * @param object $file
 *          the saved file in a Drupal file object
 *
 * @param array $validators
 *          validation functions to be run with parameters
 *
 * @return bool
 *            validation status
 *            validation errors are reported with
 *            form_set_error inside function
 */
function _webcam_field_widget_file_validator(stdClass &$file, $validators = array()) {
  $extensions = '';
  if (isset($validators['file_validate_extensions'])) {
    if (isset($validators['file_validate_extensions'][0])) {
      // Build the list of non-munged extensions if the caller provided them.
      $extensions = $validators['file_validate_extensions'][0];
    }
    else {
      // If 'file_validate_extensions' is set and the list is empty then the
      // caller wants to allow any extension. In this case we have to remove the
      // validator or else it will reject all extensions.
      unset($validators['file_validate_extensions']);
    }
  }
  else {
    // No validator was provided, so add one using the default list.
    // Build a default non-munged safe list for file_munge_filename().
    $extensions = 'jpg jpeg gif png txt doc xls pdf ppt pps odt ods odp';
    $validators['file_validate_extensions'] = array();
    $validators['file_validate_extensions'][0] = $extensions;
  }

  if (!empty($extensions)) {
    // Munge the filename to protect against possible malicious extension hiding
    // within an unknown file type (ie: filename.html.foo).
    $file->filename = file_munge_filename($file->filename, $extensions);
  }

  // Rename potentially executable files, to help prevent exploits (i.e. will
  // rename filename.php.foo and filename.php to filename.php.foo.txt and
  // filename.php.txt, respectively). Don't rename if 'allow_insecure_uploads'
  // evaluates to TRUE.
  if (!variable_get('allow_insecure_uploads', 0) && preg_match('/\.(php|pl|py|cgi|asp|js)(\.|$)/i', $file->filename) && (substr($file->filename, -4) != '.txt')) {
    $file->filemime = 'text/plain';
    $file->uri .= '.txt';
    $file->filename .= '.txt';
    // The .txt extension may not be in the allowed list of extensions. We have
    // to add it here or else the file upload will fail.
    if (!empty($extensions)) {
      $validators['file_validate_extensions'][0] .= ' txt';
      drupal_set_message(t('For security reasons, your upload has been renamed to %filename.', array('%filename' => $file->filename)));
    }
  }
  // Add in our check of the the file name length.
  $validators['file_validate_name_length'] = array();

  // Call the validation functions specified by this function's caller.
  $errors = file_validate($file, $validators);

  // Check for errors.
  if (!empty($errors)) {
    $message = t('The specified file %name could not be saved.', array('%name' => $file->filename));
    if (count($errors) > 1) {
      $message .= theme('item_list', array('items' => $errors));
    }
    else {
      $message .= ' ' . array_pop($errors);
    }
    form_set_error($file->filename, $message);
    return FALSE;
  }

  return TRUE;
}

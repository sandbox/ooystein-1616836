/**
 * @file
 * JS for loading and handling the webcam field widget
 */

/*@cc_on
(function(f){
  window.setTimeout =f(window.setTimeout);
  window.setInterval =f(window.setInterval);
})(function(f){return function(c,t){var a=[].slice.call(arguments,2);return f(function(){c.apply(this,a)},t)}});
@*/

(function($) {
  // Define Drupal.webcamFieldWidget.
  Drupal.webcamFieldWidget = Drupal.webcamFieldWidget || {};
  // Send a capture event to the flash.
  Drupal.webcamFieldWidget.buttonCapture = function($element) {
    var flashId = $element.data('flashId');
    var webcamFlash = $('#' + flashId).get(0);
    var deleteId = Drupal.settings.webcamFieldWidget.flash[flashId].deleteId;
    var canvasId = Drupal.settings.webcamFieldWidget.flash[flashId].canvasId;
    var imageFieldId = Drupal.settings.webcamFieldWidget.flash[flashId].imageFieldId;

    Drupal.settings.webcamFieldWidget.flash[flashId].deleted = false;
    $('#' + deleteId).val('FALSE');

    webcamFlash.capture(0);
    webcamFlash.save();

    var imageAsText = $('#' + canvasId).get(0).toDataURL("image/jpeg");
    $('#' + imageFieldId).val(imageAsText);
  };
  Drupal.webcamFieldWidget.buttonDelete = function($element) {
    var flashId = $element.data('flashId');
    var deleteId = Drupal.settings.webcamFieldWidget.flash[flashId].deleteId;
    var fieldsetId = Drupal.settings.webcamFieldWidget.flash[flashId].fieldsetId;

    Drupal.settings.webcamFieldWidget.flash[flashId].deleted = true;
    $('#' + deleteId).val('TRUE');
    // showImage() handles hiding image when deleted or showing this fields default image instead
    Drupal.webcamFieldWidget.showImage(flashId);

    $("#" + fieldsetId + " .webcam_field_widget_status").text(Drupal.t("Deleted"));
  };
  Drupal.webcamFieldWidget.buttonReset = function($element) {
    var flashId = $element.data('flashId');
    var deleteId = Drupal.settings.webcamFieldWidget.flash[flashId].deleteId;
    var imageFieldId = Drupal.settings.webcamFieldWidget.flash[flashId].imageFieldId;
    var fieldsetId = Drupal.settings.webcamFieldWidget.flash[flashId].fieldsetId;

    Drupal.settings.webcamFieldWidget.flash[flashId].deleted = false;
    $('#' + deleteId).val('FALSE');
    $('#' + imageFieldId).val('');
    $("#" + fieldsetId + " .webcam_field_widget_status").text(Drupal.t("Reset to original state"));
    Drupal.webcamFieldWidget.showImage(flashId);
  };
  Drupal.webcamFieldWidget.showImage = function(flashId, initialise) {
    initialise = typeof initialise !== 'undefined' ? initialise : false;
    var canvasId = Drupal.settings.webcamFieldWidget.flash[flashId].canvasId;
    var imageUri = Drupal.settings.webcamFieldWidget.flash[flashId].imageUri;
    var defaultImageUri = Drupal.settings.webcamFieldWidget.flash[flashId].defaultImageUri;
    var deleted = Drupal.settings.webcamFieldWidget.flash[flashId].deleted;
    var imageFieldId = Drupal.settings.webcamFieldWidget.flash[flashId].imageFieldId;

    Drupal.settings.webcamFieldWidget.flash[flashId].canvas = $('#' + canvasId).get(0);

    if (Drupal.settings.webcamFieldWidget.flash[flashId].canvas.getContext) {
      Drupal.settings.webcamFieldWidget.flash[flashId].ctx = Drupal.settings.webcamFieldWidget.flash[flashId].canvas.getContext("2d");
      var ctx = Drupal.settings.webcamFieldWidget.flash[flashId].ctx;
      ctx.clearRect(0, 0, 320, 240);

      var img = new Image();
      img.onload = function() {
        ctx.drawImage(this, 0, 0);
      };

      if (deleted) {
        if(defaultImageUri) {
          img.src = defaultImageUri;
        }
      }
      // If initializing, check if image has already been supplied in the html input type=hidden field
      // This occurs when form has validation errors and are redisplayed to user for correction of errors
      else if (initialise && $('#' + imageFieldId).val()) {
        img.src = $('#' + imageFieldId).val();
      }
      else if (imageUri) {
        img.src = imageUri;
      }
      else if (defaultImageUri) {
        img.src = defaultImageUri;
      }

      Drupal.settings.webcamFieldWidget.flash[flashId].image = Drupal.settings.webcamFieldWidget.flash[flashId].ctx.getImageData(0, 0, 320, 240);
    }
  };
  Drupal.webcamFieldWidget.init = function(flashId, context, run) {
    var webcamFlash = $('#' + flashId, context).get(0);
    var fieldsetId = Drupal.settings.webcamFieldWidget.flash[flashId].fieldsetId;
    var deleteId = Drupal.settings.webcamFieldWidget.flash[flashId].deleteId;

    if ($('#' + deleteId, context).val() == 'TRUE') {
      Drupal.settings.webcamFieldWidget.flash[flashId].deleted = true;
    }

    if (webcamFlash == undefined || webcamFlash.capture == undefined) {
      if (run === 0) {
          $("#" + fieldsetId + " .webcam_field_widget_status", context).text(Drupal.t("error: Flash movie not yet registered!"));
      }
      else {
        /* Flash interface not ready yet */
        window.setTimeout(Drupal.webcamFieldWidget.init,  1000 * (4 - run), flashId, context, run - 1);
      }
    } else {
      // webcam.onSave(imageCollumn) is called from the flash
      // one call for each column
      var webcam = {
        pos: 0,
        onSave: function (data) {
          var col = data.split(";");
          var img = Drupal.settings.webcamFieldWidget.flash[flashId].image;
          var tmp = 0;
          for(var i = 0; i < 320; i++) {
            tmp = parseInt(col[i]);
            img.data[this.pos + 0] = (tmp >> 16) & 0xff;
            img.data[this.pos + 1] = (tmp >> 8) & 0xff;
            img.data[this.pos + 2] = tmp & 0xff;
            img.data[this.pos + 3] = 0xff;
            this.pos += 4;
          }
          // 0x4B000 = 320 * 240 * (3 colors + 1 alpha layer)
          if (this.pos >= 0x4B000) {
            Drupal.settings.webcamFieldWidget.flash[flashId].ctx.putImageData(img, 0, 0);
            Drupal.settings.webcamFieldWidget.flash[flashId].image = img
            this.pos = 0;
          }
        },
        debug: function (type, string) {
          $("#" + fieldsetId + " .webcam_field_widget_status").text(type + ": " + string);
        }
      };

      var cams = webcamFlash.getCameraList();
      for(var i in cams) {
        $("#" + fieldsetId + " .webcam_field_widget_cams", context).once('cameralist', function() {
          $("#" + fieldsetId + " .webcam_field_widget_cams", context).append("<li>" + cams[i] + "</li>");
        });
      }
      window[flashId] = webcam;
      Drupal.webcamFieldWidget.showImage(flashId, true);
    }
  };
  // Add the flash to the page.
  Drupal.behaviors.webcamFieldWidget = {
    attach: function(context, settings) {
      for (var flashId in settings.webcamFieldWidget.flash) {
        var fieldsetId = settings.webcamFieldWidget.flash[flashId].fieldsetId;
        var webcamPath = settings.webcamFieldWidget.flash[flashId].webcamPath;
        var imageUri = settings.webcamFieldWidget.flash[flashId].imageUri;

        // Bind the buttons
        $('#' + fieldsetId + ' .media-webcam-snap', context).once('media-webcam-snap', function() {
          $(this).bind('click', function(e) {
              Drupal.webcamFieldWidget.buttonCapture($(this));
            })
          .data('flashId', flashId);
        });

        $('#' + fieldsetId + ' .media-webcam-delete', context).once('media-webcam-delete', function() {
          $(this).bind('click', function(e) {
              Drupal.webcamFieldWidget.buttonDelete($(this));
            })
          .data('flashId', flashId);
        });
        $('#' + fieldsetId + ' .media-webcam-reset', context).once('media-webcam-reset', function() {
          $(this).bind('click', function(e) {
              Drupal.webcamFieldWidget.buttonReset($(this));
            })
          .data('flashId', flashId);
        });

        // Replace the empty div with the Flash, using swfobject.
        var flashVars = {
          flashid: flashId,
          mode:"callback",
          quality:"100"
        };
        var attributes = {
          id: flashId
        };
        swfobject.embedSWF(webcamPath, flashId, 320, 240, "10.0.0", false, flashVars, false, attributes);
        Drupal.webcamFieldWidget.init(flashId, context, 3);
      }
    }
  };
})(jQuery);

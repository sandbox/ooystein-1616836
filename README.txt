-- REQUIREMENTS --
jQuery 1.7 or greater.

-- INSTALLATION --
* Install as usual.

-- CONFIGURATION -
For changing widget on an existing field:
* Go to the admin page for the content type. 
* Enter the manage fields page.
* Klick on the field widget the field is using (Default is "Image").
* Find "Webcam" in the widget dropdown list.

When creating new field:
* Go to the admin page for the content type.
* Add an image field on the content type.
* Select Webcam as the widget for the field.


To compile flash in linux::
apt-get install swfmill mtasc
make